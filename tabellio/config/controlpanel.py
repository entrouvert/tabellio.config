import logging

from zope.component import getUtility
from zope.component import getMultiAdapter
from zope.publisher.browser import BrowserView

from plone.resource.utils import queryResourceDirectory

from plone.registry.interfaces import IRegistry

from tabellio.config.interfaces import _
from tabellio.config.interfaces import ITabellioSettings

from AccessControl import Unauthorized
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.decode import processInputs
from Products.statusmessages.interfaces import IStatusMessage

logger = logging.getLogger('tabellio.config')

class TabellioControlpanel(BrowserView):

    def __call__(self):
        if self.update():
            return self.index()
        return ''

    def _setup(self):
        self.settings = getUtility(IRegistry).forInterface(ITabellioSettings, False)

    def update(self):
        processInputs(self.request)
        self._setup()
        self.errors = {}
        submitted = False
        form = self.request.form

        if 'form.button.Cancel' in form:
            self.redirect(_(u"Changes canceled."))
            return False

        if 'form.button.LocationsSave' in form:
            self.authorize()
            submitted = True

            deputies_path = form.get('deputiesPath')
            persons_path = form.get('personsPath')
            ministries_path = form.get('ministriesPath')
            documents_path = form.get('documentsPath')
            convocations_path = form.get('convocationsPath')
            dossiers_path = form.get('dossiersPath')
            questions_path = form.get('questionsPath')
            polgroups_path = form.get('polgroupsPath')
            parlevents_path = form.get('parleventsPath')
            commissions_path = form.get('commissionsPath')
            generalagenda_path = form.get('generalagendaPath')

            if not self.errors:
                self.settings.deputiesPath = deputies_path
                self.settings.personsPath = persons_path
                self.settings.ministriesPath = ministries_path
                self.settings.documentsPath = documents_path
                self.settings.convocationsPath = convocations_path
                self.settings.dossiersPath = dossiers_path
                self.settings.questionsPath = questions_path
                self.settings.polgroupsPath = polgroups_path
                self.settings.parleventsPath = parlevents_path
                self.settings.commissionsPath = commissions_path
                self.settings.generalagendaPath = generalagenda_path

        if 'form.button.SessionsSave' in form:
            self.authorize()
            submitted = True
            self.settings.sessions = form.get('sessions')

        if 'form.button.TopicsSave' in form:
            self.authorize()
            submitted = True
            self.settings.topics = form.get('topics')

        if 'form.button.GreffierSave' in form:
            self.authorize()
            submitted = True
            self.settings.greffier_name = form.get('greffier_name')
            self.settings.greffier_email = form.get('greffier_email')
            self.settings.greffier_address_line1 = form.get('greffier_address_line1')
            self.settings.greffier_address_line2 = form.get('greffier_address_line2')
            self.settings.greffier_phone = form.get('greffier_phone')
            self.settings.greffier_fax = form.get('greffier_fax')

        if 'form.button.MiscSave' in form:
            self.authorize()
            submitted = True
            self.settings.ical_username = form.get('ical_username')
            self.settings.ical_password = form.get('ical_password')
            self.settings.audiofiles_path = form.get('audiofiles_path')
            self.settings.commission_audio_codes = form.get('commission_audio_codes')
            self.settings.embedded_audio_player_url = form.get('embedded_audio_player_url')
            self.settings.live_stream_url = form.get('live_stream_url')
            self.settings.contest_subject_email = form.get('contest_subject_email')
            self.settings.contest_body_email = form.get('contest_body_email')
            self.settings.gender_colors = form.get('gender_colors')
            self.settings.ageranges_color = form.get('ageranges_color')

        if submitted and not self.errors:
            self._setup()
            IStatusMessage(self.request).add(_(u"Changes saved"))
        elif submitted:
            IStatusMessage(self.request).add(_(u"There were errors"), 'error')

        return True

    def authorize(self):
        authenticator = getMultiAdapter((self.context, self.request), name=u"authenticator")
        if not authenticator.verify():
            raise Unauthorized

    def redirect(self, message):
        IStatusMessage(self.request).add(message)
        portalUrl = getToolByName(self.context, 'portal_url')()
        self.request.response.redirect("%s/plone_control_panel" % portalUrl)
