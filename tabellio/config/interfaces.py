from zope.interface import Interface
from zope import schema
from zope.i18nmessageid import MessageFactory

_ = MessageFactory(u'tabellio.config')

class ITabellioSettings(Interface):
    '''Tabellio Settings'''

    commissionsPath = schema.TextLine(
        title=_('commissions_path', 'Commissions Path'),
        required=False)

    deputiesPath = schema.TextLine(
        title=_('deputies_path', 'Deputies Path'),
        required=False)

    convocationsPath = schema.TextLine(
        title=_('convocations_path', 'Convocations Path'),
        required=False)

    documentsPath = schema.TextLine(
        title=_('documents_path', 'Documents Path'),
        required=False)

    dossiersPath = schema.TextLine(
        title=_('dossiers_path', 'Dossiers Path'),
        required=False)

    questionsPath = schema.TextLine(
        title=_('questions_path', 'Questions Path'),
        required=False)

    ministriesPath = schema.TextLine(
        title=_('ministries_path', 'Ministries Path'),
        required=False)

    parleventsPath = schema.TextLine(
        title=_('parlevents_path', 'Parlementary Events Path'),
        required=False)

    personsPath = schema.TextLine(
        title=_('persons_path', 'Persons Path'),
        required=False)

    polgroupsPath = schema.TextLine(
        title=_('polgroups_path', 'Political Groups Path'),
        required=False)

    generalagendaPath = schema.TextLine(
        title=_('generalagenda_path', 'General Agenda Path'),
        required=False)

    sessions = schema.Text(
        title=_('sessions', 'Sessions'),
        required=False)

    topics = schema.Text(
        title=_('topics', 'Topics'),
        required=False)

    greffier_name = schema.Text(title=_(u'Greffier Name'), required=False)
    greffier_email = schema.Text(title=_(u'Greffier Email'), required=False)
    greffier_address_line1 = schema.Text(title=_(u'Greffier Address Line 1'), required=False)
    greffier_address_line2 = schema.Text(title=_(u'Greffier Address Line 2'), required=False)
    greffier_phone = schema.Text(title=_(u'Greffier Phone'), required=False)
    greffier_fax = schema.Text(title=_(u'Greffier Fax'), required=False)

    ical_username = schema.TextLine(
        title=_('ical_username', 'ICal Username'),
        required=False)

    ical_password = schema.TextLine(
        title=_('ical_password', 'ICal Password'),
        required=False)

    audiofiles_path = schema.TextLine(
        title=_(u'Audio Files Path'),
        required=False)

    commission_audio_codes = schema.Text(title=_(u'Commission Audio Codes'), required=False)
    embedded_audio_player_url = schema.Text(title=_(u'Embedded Audio Player URL'), required=False)
    live_stream_url = schema.Text(title=_(u'Live stream URL'), required=False)

    contest_subject_email = schema.TextLine(
        title=_(u'Contest Subject Email'),
        required=False)

    contest_body_email = schema.Text(title=_(u'Contest Subject Email'), required=False)

    gender_colors = schema.Text(title=_(u'Gender Colors'), required=False)
    ageranges_color = schema.Text(title=_(u'Age Ranges Colors'), required=False)
