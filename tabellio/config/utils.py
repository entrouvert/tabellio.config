# -*- coding: utf-8 -*-

import time

from zope import component
from plone.registry.interfaces import IRegistry
from interfaces import ITabellioSettings
from plone.memoize import ram

def get_dossiers_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).dossiersPath

def get_questions_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).questionsPath

def get_documents_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).documentsPath

def get_convocations_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).convocationsPath

def get_deputies_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).deputiesPath

def get_polgroups_path():
    return component.getUtility(IRegistry).forInterface(ITabellioSettings, False).polgroupsPath

def get_sessions():
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    sessions = settings.sessions
    if not sessions:
        return []
    terms = [x.split(':')[1].strip() for x in sessions.splitlines() if x]
    return terms

@ram.cache(lambda *args: time.time() // (5 * 60)) # 5 minutes
def get_topics_dict():
    from plone.i18n.normalizer.fr import normalizer
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    if not settings.topics:
        return {}
    topics = {}
    for line in settings.topics.splitlines():
        if line.count('|') == 1:
            id, term = line.strip().split('|')
        else:
            term = line.strip()
            id = term.encode('ascii', 'replace')
        topics[id] = (term, normalizer.normalize(term).lower())
    return topics

def get_legisl_and_sessions():
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    sessions = settings.sessions
    if not sessions:
        return []
    terms = []
    current_legisl = None
    for term in sessions.splitlines():
        legisl, session = term.strip().split(':')
        if legisl != current_legisl:
            terms.append(legisl)
            current_legisl = legisl
        terms.append('-- %s' % session)
    return terms

def get_list_of_sessions(legisl_or_session):
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    if not legisl_or_session.startswith('--'):
        return [x.split(':')[1].strip() for x in settings.sessions.splitlines() if x.split(':')[0] == legisl_or_session]
    else:
        return [legisl_or_session[3:]]

def get_ordered_sessions():
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    return list(reversed([x.split(':')[1].strip() for x in settings.sessions.splitlines()]))

def get_com_audio_code(com_id):
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    for line in settings.commission_audio_codes.splitlines():
        id, audio_code = line.strip().split(':')
        if id == com_id:
            return audio_code
    return None
